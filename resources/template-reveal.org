#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+DATE: xx-xx-22
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/github.min.css
#+REVEAL_EXTRA_CSS: ../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="text-transform:uppercase">%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><h4>%d</h4><br><p>Escuela Superior de Diseño de Madrid</p>

* Resumen
- Resumen 1
- Resumen 2
* Part 1
#+begin_src processing

#+end_src
* Part 2
#+reveal: split:t
#+reveal: split:t
* Part 3
#+reveal: split:t
#+reveal: split:t
* Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 50%">
Column1
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 50%">
Column 2
#+REVEAL_HTML: </div>
