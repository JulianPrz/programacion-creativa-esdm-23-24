ArrayList <Ball> balls; //creamos una lista de tamaño flexible que se llama balls

void setup() {
  size(600, 600);
  balls = new ArrayList();
  for (int i = 0; i < 20; i++) { //Repetimos 20 veces
    Ball b = new Ball(random(width), random(height), 15, color(255)); //Crea una nueva bola
    balls.add(b); //Añádela a la lista
  }
}

void mousePressed(){
  Ball b = new Ball(random(width), random(height), 15, color(255)); //Crea una nueva bola
  balls.add(b);
}

void keyPressed(){
  if(balls.size() >=1)
    balls.remove(balls.size()-1);
}

void draw() {
  background(0);
  for (int i = 0; i < balls.size(); i++) { // iteramos en función del tamaño de nuestra lista para mostrar todas las bolas
    Ball b = balls.get(i); //Llama a cada bola de nuestra lista
    b.display(); 
    b.move();
    b.checkEdges();
  }

  //for (Ball b : balls) { //Pasa por cada bola b de nuestra lista balls
  //  b.display(); 
  //  b.move();
  //  b.checkEdges();
  //}
}
