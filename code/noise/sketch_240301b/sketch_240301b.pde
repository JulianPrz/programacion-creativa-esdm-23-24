float x;
float tx=1;
float y;
float ty=2;
float s;
float ts = 3;
float col;
float tcol = 4;

void setup() {
  //size(300,300);
  fullScreen();
  background(0);
  noStroke();
}

void draw() {
  fill(0, 10);
  rect(0, 0, width, height);
  //noise eje X
  x = noise(tx);
  println("El valor de x es: "+x);
  x = map(x, 0, 1, 0, width);
  //noise eje Y
  y = noise(ty);
  println("El valor de y es: "+y);
  y = map(y, 0, 1, 0, height);
  //noise tamaño ellipse
  s = noise(ts);
  println("El valor de s es: "+s);
  s = map(s, 0, 1, 10, 200);

  //noise color pelota
  col = noise(tcol);
  println("El valor de col es: "+col);
  col = map(col, 0, 1, 0, 255);

  //stroke(255);
  fill(col, 145, 245);
  ellipse(x, y, s, s);
  tx+=0.001;
  ty+=0.001;
  ts+=0.005;
  tcol+=0.005;
}
