float xPos; //Trabajamos mejor con floats
float yPos;
float xVel = 2.8;
float yVel = 2.2;
int xDir = 1;
int yDir = 1;
int r = 25;

void setup() {
  size(300, 300);
  xPos=width/2;
  yPos=height/2;
}

void draw() {
  background(0);
  muestraBola();
  mueveBola();
  tocaBorde();
}

void muestraBola() {
  ellipse(xPos, yPos, r*2, r*2);
}

void mueveBola() {
  xPos = xPos + (xVel * xDir);
  yPos = yPos + (yVel * yDir);
}

void tocaBorde() {
  if (xPos > width-r || xPos < 0+r) {
    xDir *= -1;
  }

  if (yPos < 0+r || yPos > height-r) {
    yDir *= -1;
  }
}
