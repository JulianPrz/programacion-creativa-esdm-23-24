size(300, 300);

for (int circleY = 75; circleY <= 225; circleY += 75) {
  for (int circleX = 75; circleX <= 225; circleX += 75) {
    ellipse(circleX, circleY, 50, 50);
  }
}
