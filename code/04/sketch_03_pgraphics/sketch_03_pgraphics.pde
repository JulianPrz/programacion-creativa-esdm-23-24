PGraphics pg1, pg2;
int currentLayer = 1;

void setup() {
  size(600, 600);
  pg1=createGraphics(width, height);
  pg2=createGraphics(width, height);
}

void draw() {
  background(0);

  if (currentLayer == 1) {
    pg1.beginDraw();
    pg1.fill(160, 200, 0);
    pg1.noStroke();
    if (mousePressed) {
      pg1.ellipse(mouseX, mouseY, 40, 40);
    }
    pg1.endDraw();
  } else if (currentLayer==2) {
    pg2.beginDraw();
    pg2.fill(150, 20,120);
    pg2.noStroke();
    if (mousePressed) {
      pg2.ellipse(mouseX, mouseY, 40, 40);
    }
    pg2.endDraw();
  }

  image(pg1, 0, 0);
  image(pg2, 0, 0);
  
}
void keyPressed() {
  if (key=='1') {
    currentLayer = 1;
  } else if (key=='2') {
    currentLayer = 2;
  }
}
