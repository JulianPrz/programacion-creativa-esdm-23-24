color[] col = new color[3];
int ind = 0;

void setup() {
  size(200, 200);
  for (int i=0; i < col.length; i++) {
    col[i] = color(random(255), random(255), random(255));
  }
}

void draw() {
}
void mousePressed() {

  fill(col[ind]);
  println(col[ind]);
  println(ind);
  ellipse(mouseX, mouseY, 30, 30);
  ind++;
  if (ind == col.length) {
    ind=0;
  }
}
