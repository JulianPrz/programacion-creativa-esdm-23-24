PFont f;
float a = 0;
char c = 'A';
float n = 3;

void setup() {
  size(800, 800);
  background(0);
  f = createFont("Iosevka", 48); //Aquí tu tipografía favorita
  textFont(f, 200);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0);
  pushMatrix();
  translate(width/2, height/2);
  for (int i = 0; i <= n; i++) {
    a = 360/n;
    rotate(radians(a));
    text(c, 0, 0);
  }
  popMatrix();
}
