String[] parrafos;
String textoEntero;
String[] words;
int inc = 0;
float x = 0;

void setup() {
  size(800, 400);
  background(0);
  parrafos = loadStrings("quijote.txt");
  textoEntero = join(parrafos, " ");
  words = splitTokens(textoEntero, " .,;?!\"():¿¡");
  printArray(words);
  //textAlign(CENTER, CENTER);
  textSize(50);
  frameRate(5);
}

void draw() {
  x = 0;
  background(0);
  text(words[inc], x, height/2);
  x = x + textWidth(words[inc])+20;
  text(words[inc+1], x, height/2);
  x = x + textWidth(words[inc+1])+20;
  text(words[inc+2], x, height/2);
  x = x + textWidth(words[inc+1])+20;
  inc+=3;
}
