PFont f;
float a = 0;
String st = "ola";
float n = 3;
float x = 0;
float tx = 1;
float tn = 2;



void setup() {
  size(800, 800);
  background(0);
  f = createFont("Iosevka", 48); //Aquí tu tipografía favorita
  textFont(f, 200);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0);
  n = noise(tn);
  n = round(map(n, 0, 1, 3, 10));
  x = noise(tx);
  x = map(x, 0, 1, -width/2, width/2);
  pushMatrix();
  translate(width/2, height/2);
  for (int i = 0; i <= n; i++) {
    a = 360/n;
    rotate(radians(a));
    text(st, x, 0);
  }
  popMatrix();
  tx += 0.005;
  tn += 0.005;
}


void keyTyped() {
  if (key >= '0' && key <= 'z') {
    char c = char(int(random(33, 127)));
    st = st + c;
  }

  if (st.length() >= 1) {
    if (key ==BACKSPACE) {
      st = st.substring(0, st.length()-1);
    }
  }
}
