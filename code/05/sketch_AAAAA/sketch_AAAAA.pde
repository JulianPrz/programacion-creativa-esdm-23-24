PFont f;
int w = 0;
int h = 3;
float a = 0;
char c = 'A';
float n = 0;

void setup() {
  size(800, 800);
  background(0);
  f = createFont("Iosevka", 48); //Aquí tu tipografía favorita
  textFont(f, 200);
}

void draw() {
  background(0);
  for (int i = 0; i < n; i++) {
    text(c, i*textWidth(c), height/2);
  }
  n = map(mouseX, 0, width, 0, 10);
}
