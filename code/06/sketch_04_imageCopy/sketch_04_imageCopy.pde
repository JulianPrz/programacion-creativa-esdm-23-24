PImage img;

void setup() {
  size(400, 400);
  img = loadImage("https://upload.wikimedia.org/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg");
  img.resize(width, height);
  background(0);
}

void draw() {
  //image(img, 0, 0, 400, height);
  int x = int(random(width));
  int y = int(random(height));
  copy(img, x, y, int(random(50)), int(random(50)), x, y, int(random(50)), int(random(50))); 
}
