PImage img;
int x;
int y;
int gap = 10;

void settings() {
  img= loadImage("https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/ff5604ba-57a5-48f8-a91f-85c1de625100/d4xc2j2-50178a00-e68d-45ea-8430-9ca107860e7f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2ZmNTYwNGJhLTU3YTUtNDhmOC1hOTFmLTg1YzFkZTYyNTEwMFwvZDR4YzJqMi01MDE3OGEwMC1lNjhkLTQ1ZWEtODQzMC05Y2ExMDc4NjBlN2YuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.4L6I24PbyYROnGFCPTovTDUR1_1ecMJQ82fT-VV63QU", "jpg"); // Cargamos la imagen
  size(img.width, img.height); // Tamaño del canvas adaptado al tamaño de la imagen
}

void setup() {
  //image(img, 0,0);
  img.loadPixels();
  println(img.width);
  println(img.height);
  println(img.pixels.length);
  noStroke();

  for (int i = 0; i < img.pixels.length; i = i +gap) {
    x = i % img.width;
    y = i / img.width;
    //println(x, y);
    fill(img.pixels[i]);
    ellipse(x, y, gap, gap);
  }
  updatePixels();
}

void draw() {
}
