PImage img;
int x;
int y;
int gap = 5;
void settings() {
  img = loadImage("https://www.aranzadi.eus/assets/img/pages/flores-bonitas-230509131129.jpg");
  size (img.width, img.height);
}
void setup() {
  image(img, 0, 0);
}
void draw() {
  int x = int(random(width));
  int y = int(random(height));
  copy(img, x, y, int(random(100)), int(random(100)), int(random(width)), int(random(height)), int(random(100)), int(random(100)));
}
