import ddf.minim.analysis.*;
import ddf.minim.*;

Minim         minim;
AudioPlayer   player;
AudioMetaData meta;
FFT           fft;

void setup(){
  size(512, 256, FX2D);
  minim = new Minim(this);
  player = minim.loadFile("04. Who.mp3", 1024);
  meta = player.getMetaData();
  fft = new FFT( player.bufferSize(), player.sampleRate() );
  player.loop();
}

void draw(){
  //METADATA
  fill(0, 127, 0);
  text("Título: " + meta.title(), width*0.6, height*0.7);
  text("Álbum: " + meta.album(), width*0.6, height*0.8);
  text("Artista: " + meta.author(), width*0.6, height*0.9);
  noStroke();
  fill(0, 20);
  rect(0, 0, width, height);
  strokeWeight(2);
  
  //CANAL IZQ, ARRIBA, ROJO
  fft.forward( player.left );
  int w = 6;
  stroke(255, 0, 0, 50);
  for (int i = 0; i < fft.specSize(); i++){
    line( 20+i*w, height/2, 20+i*w, constrain(height/2 - fft.getBand(i*w)*4, 20, height/2));
  }
  
  //CANAL DER, ABAJO, VERDE
  fft.forward( player.right);
  stroke(0, 255, 0, 50);
  for (int i = 0; i < fft.specSize(); i++){
    line(20+i*w, height/2, 20+i*w, constrain(height/2 + fft.getBand(i*w)*4, height/2, height-20));
  }
  
  //NIVEL AUDIO, CANAL IZQ, ROJO
  noStroke();
  fill(127, 0, 0);
  rect( width*0.6, height*0.1, player.left.level()*width*0.2, height*0.02 );
  stroke(127, 0, 0);
  noFill();
  rect( width*0.6, height*0.1, width*0.2, height*0.02 );
  
  //NIVEL AUDIO, CANAL DER, VERDE
  noStroke();
  fill(0, 127, 0);
  rect( width*0.6, height*0.15, player.right.level()*width*0.2, height*0.02 );
  stroke(0, 127, 0);
  noFill();
  rect( width*0.6, height*0.15, width*0.2, height*0.02 );
}
