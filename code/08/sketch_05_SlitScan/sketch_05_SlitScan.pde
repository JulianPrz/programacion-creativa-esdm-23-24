import processing.video.*; //Importamos la librería de video

Capture video;
float inc=0;
void setup() {
  size(640, 240);
  video = new Capture(this, Capture.list()[0]); //Esta es otra forma de llamar a las diferentes 
  video.start(); //Iniciamos el video
  background(0);
}

void captureEvent(Capture video){ 
  video.read();
}

void draw() {
  if(inc>=width){
    inc=0;
  }
  copy(video,video.width/2,0,2,height,(int)inc,0,2,height);
  inc+=0.5;
}
