import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer song;
BeatDetect beat;

float r;

void setup() {
  size(200, 200);
  minim = new Minim(this);
  song = minim.loadFile("audio.mp3", 2048);
  song.play();
  beat = new BeatDetect();
  r = 20;
}

void draw() {
  background(0);
  beat.detect(song.mix);
  float a = map(r, 20, 80, 60, 255);
  fill(0, 255, 0, a);
  if (beat.isOnset()) {
    r = 80;
  }
  ellipse(width/2, height/2, r*2, r*2);
  r-=2; //Va reduciéndose
  if ( r < 20 ) {
    r = 20; //Si lleva a 20 se queda ahí
  }
}
