#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE: E1 Diseño paramétrico
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="text-transform:uppercase;font-size:2em" >%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><h4>%d</h4><br><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Enunciado
#+HTML: <img src="./materiales/parametrico.svg" width="600" height="350"/>
*Diseño paramétrico*
#+reveal: split:t
- Se trata de crear una serie de _personajes/objetos/logo/loquequieras paramétrico_
- Es importante primero definirlo en papel en una rejilla y *utilizar variables* para dibujarlo
#+reveal: split:t
- Con la función =map()= transformaremos algún parámetro como el tamaño, color, posición, etc.
- Pulsando teclas asignadas cambiaremos de personaje
- [[https://gitlab.com/JulianPrz/programacion-creativa-esdm-23-24/-/tree/main/docs/04_Practicas/E1_parametrico/materiales?ref_type=heads][En esta carpeta]] encontrarás los materiales para completar el ejercicio
* Requisitos
  - Comentarios al inicio del sketch de lo que hace tu código
  - Tamaño del canvas 800x400 píxeles
  - Elaborar al menos 2 personajes/objetos/logo/loquequieras
  - Mapear al menos el parámetro de tamaño con =mouseX= o =mouseY= 
* Evaluación
  - Se valorará la calidad del código, los comentarios, ideación, estética, cumplir con los requisitos y la entrega a tiempo.
* Entrega
- La entrega finaliza el día *1 de marzo* y se pueden resolver dudas en las siguientes sesiones presenciales y por Slack
- Se entrega a través del campus virtual con la siguiente nomenclatura: ="E1_apellido_nombre.pde"=
- Si se quiere entregar algún documento de más, la entrega tiene que realizarse con un único archivo comprimido
  
