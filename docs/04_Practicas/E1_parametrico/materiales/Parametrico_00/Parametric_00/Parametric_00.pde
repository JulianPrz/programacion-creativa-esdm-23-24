float x, y, w;

void setup() {
  size(150, 150);
  
  x = width/2;
  y = height/2;
  w = width;
}

void draw() {
  background(255);
  
  noStroke();
  
  // body
  fill(0, 200, 0);
  ellipse(x, y, w, w);
  // eye
  fill(255);
  ellipse(x, y-w/8, w/2, w/2);
  
  fill(0);
  ellipse(x, y-w/8, w/4, w/4);
}
