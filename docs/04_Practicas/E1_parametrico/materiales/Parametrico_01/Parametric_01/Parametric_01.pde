float x, y, w;
String name;

void setup() {
  size(600, 300);

  x = 0;
  y = 150;
  w = 150;

  name = "Mike";
  
}

void draw() {
  background(255);

  noStroke();

  // Mike
  if (name.equals("Mike")) {
    // body
    fill(0, 200, 0);
    ellipse(x, y, w, w);

    // eye
    fill(255);
    ellipse(x, y-w/8, w/2, w/2);

    fill(0);
    ellipse(x, y-w/8, w/4, w/4);
  }

  // Sullivan
  if (name.equals("Sullivan")) {
    // body
    fill(0, 0, 200);
    rectMode(CENTER);
    rect(x, y, w, w);

    // eye
    fill(255);
    ellipse(x, y-w/8, w/2, w/2);

    fill(0);
    ellipse(x, y-w/8, w/4, w/4);
  }


  if (name.equals("Ronald")) {
    // body
    fill(200, 0, 0);
    beginShape();
    vertex(x-w/2, y-w/2);
    vertex(x+w/2, y-w/2);
    vertex(x+w/4,  y+w/2);
    vertex(x-w/4,  y+w/2);
    endShape(CLOSE);

    // eye
    fill(255);
    ellipse(x, y-w/8, w/2, w/2);

    fill(0);
    ellipse(x, y-w/8, w/4, w/4);
  }

  x = mouseX;
  w = map(mouseY, 0, height, 24, 500);
  
  fill(0);
  textSize(12);
  textAlign(CENTER, CENTER);
  text("press M / S / R", width/2, 18);
  text(name, width/2, 32);
}

void keyPressed() {
  if (key == 'm') {
    name = "Mike";
  }

  if (key == 's') {
    name = "Sullivan";
  }

  if (key == 'r') {
    name = "Ronald";
  }
}
