#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE: E3 Recode
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+DATE: Entrega 25-04-22
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight)
# #+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/github.min.css
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: https://julianprz.gitlab.io/computacion-fisica-21-22/master/assets/css/modifications.css
#+REVEAL_EXTRA_CSS: https://julianprz.gitlab.io/computacion-fisica-21-22/master/assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="text-transform:uppercase;font-size:2em" >%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><h4>%d</h4><br><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Enunciado
#+HTML: <img src="https://www.101computing.net/wp/wp-content/uploads/vera-molnar-artwork.png" height="250"/>
*Recode*
#+reveal: split:t
- Esta práctica está inspirada en diferentes iniciativas realizadas en la comunidad de la programación creativa
- La más signiticativa es [[http://recodeproject.com/][The ReCode Project]] que actualiza piezas expuestas en al revista "Computer and graphics art" al lenguaje Processing
#+reveal: split:t
- Se trata de escoger una obra de arte, ya sea de computer art o de arte óptico, y escribir el código para que el resultado sea similar
- La elección de la obra tiene que estar condicionada porque la pieza tenga un aspecto "codificable". Es decir, que esté mayormente basado en formas geométricas, que haya repeticiones, etc.
* Requisitos
- Comentarios al inicio del sketch de lo que hace tu código
- Indicar autor/a, nombre de la pieza original, breve descripción, decisión de la elección, etc.
- Conseguir un aspecto similar
- Que sea generativo, es decir, cada vez que se ejecute o a través de una interacción el resultado varie
* Referencias
- Pioneras: [[https://nodisparenalartista.wordpress.com/2013/03/26/hilma-af-klint/][Hilma Af Klint]], [[http://temporaryartreview.com/hilma-af-klint-and-emma-kunz-conscious-collaboration-with-spirit/][Emma Kunz]],  [[https://albersfoundation.org/art/anni-albers/prints/index/][Annie Albers]], [[https://www.tate.org.uk/search?aid=1845&type=artwork][Bridget Riley]], [[https://www.wikiart.org/en/lygia-pape][Lygia Pape]], [[https://proyectoidis.org/lillian-schwartz/][Lillian F. Schwartz]], [[http://www.veramolnar.com/][Vera Molnar]], Muriel Cooper
- [[http://www.op-art.co.uk/2016/03/victor-vasarely-the-french-institute-budapest-hungary/][Victor Vassarely]], [[http://www.yturralde.org/Paginas/Etapas/et04/et0411-es.html][Yturralde]], [[https://www.invaluable.com/auction-lot/ulrichs-timm-visuelle-konstruktion-siebdruck-2292-c-2d54f35a17][Timms Ulrich]], [[https://oa.letterformarchive.org/search?dims=Collection&vals0=Aaron%20Marcus%20Collection][Aaron Marcus]], John & James Whitney, Sol Lewitt
- Arte óptico (OpArt), Arte conceptual, Arte minimalista, Arte Concreto
* Iniciativas similares
- http://recodeproject.com/
- http://creativecodingmadrid.com/galeria-recode
- http://sfpc.io/recoded/
- https://hex6c.medium.com/generative-art-recoded-675c3eabb2f2
* Evaluación
  - Se valorará la calidad del código, los comentarios, ideación, estética, cumplir con los requisitos y la entrega a tiempo.
* Entrega
- La entrega finaliza el día *25 de marzo* y se pueden resolver dudas en las siguientes sesiones presenciales y por correo
- Se entrega a través del campus virtual con la siguiente nomenclatura: ="E3_apellido_nombre.pde"=
- Si se quiere entregar algún documento de más, la entrega tiene que realizarse con un único archivo comprimido /.zip/
  
