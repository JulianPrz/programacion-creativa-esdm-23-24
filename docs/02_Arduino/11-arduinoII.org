#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
#+OPTIONS: ^:nil
:END:  

#+TITLE:Arduino II
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+DATE: 06-04-22
#+LANGUAGE: es
#+EMAIL:julian.perezromero@educa.madrid.org
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="font-size:2em" >%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><br><p>Escuela Superior de Diseño de Madrid</p>
#+REVEAL_INIT_OPTIONS: hash: true
#+OPTIONS: toc:nil

* Índice
  - [[Pulsador PULL-UP][Pulsador PULL-UP]]
  - [[Debounce][Debounce]]
  - [[delay vs millis][delay vs millis]]
  - [[Servomotores][Servomotores]]
  - [[Memoria EEPROM][Memoria EEPROM]]
  - [[LCD][LCD]]
* Resumen
- Veremos cómo usar correctamente un pulsador con un circuito /PullUp/
- Trabajaremos el tiempo de forma asíncrona con =millis()=
- Aprenderemos qué son los servomotores y a moverlos con el código
- Veremos cómo usar la memoria EEPROM de nuestro Arduino
* Pulsador PULL-UP
- Cuando usamos un pulsador y lo definimos como INPUT vemos que la señal no es estable
- Hacemos la prueba conectando el pulsador y subiendo el código
#+reveal: split
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :height 400px :display block
#+caption: 
[[../../img/11/pulsador.png]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+begin_src arduino
int val;
void setup() {
  Serial.begin(9600);
  pinMode(7,INPUT);
}

void loop() {
  val=digitalRead(7);
  if(val == HIGH){
    Serial.println("1");
    }else{
      Serial.print("0");
    }
}
#+end_src
#+REVEAL_HTML: </div>

#+reveal: split
Conectando un led lo veremos mejor
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :height 400px :display block
#+caption: 
[[../../img/11/pulsador2.png]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+begin_src arduino
int val;
void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(7,INPUT);
}

void loop() {
  val=digitalRead(7);
  if(val == HIGH){
    digitalWrite(13,HIGH);
    Serial.println("1");
    }else{
      digitalWrite(13,LOW);
      Serial.print("0");
    }
}
#+end_src
#+REVEAL_HTML: </div>
#+reveal: split
¿Por qué ocurre esto? --> ENTRADA FLOTANTE
#+attr_html: :height 400px :display block
#+caption: Esquema pull-up pull-down. Fuente: [[https://www.youtube.com/watch?v=BdWMFMXYIHw][Tinkerall]]
[[../../img/11/pullup1.png]]
#+reveal: split
¿Cómo podemos hacer para que esto no ocurra? Circuit pull-up/pull-down
#+attr_html: :height 300px :display block
#+caption: Esquema pull-up pull-down. Fuente: [[https://www.youtube.com/watch?v=BdWMFMXYIHw][Tinkerall]]
[[../../img/11/pullup2.png]]
#+reveal: split
- Tenemos una solución más fácil
- El chip ATmega de Arduino trae integradas resistencias de 20Kohm que podemos utilizar para definir los puertos como entradas con resistencia Pull-Up
- Para ello tendremos que definirlo en el código: src_arduino[:exports code]{pinMode(7, INPUT_PULLUP);}
#+reveal: split
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/11/pulsador2.png]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+begin_src arduino
int val;
void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(7,INPUT_PULLUP);
}

void loop() {
  val=digitalRead(7);
  if(val == HIGH){
    digitalWrite(13,HIGH);
    Serial.println("1");
    }else{
      digitalWrite(13,LOW);
      Serial.print("0");
    }
}
#+end_src
#+REVEAL_HTML: </div>
* Debounce
#+begin_src arduino
int buttonPin = 2;
int ledPin = 13;
bool buttonState = false;
int pressed = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
}

void loop() {
  if (debounce(buttonState) == HIGH && buttonState == LOW) {
    pressed++;
    buttonState = HIGH;
  } else if (debounce(buttonState) == LOW && buttonState == HIGH) {
    buttonState = LOW;
  }
  if (pressed == 10) {
    digitalWrite(ledPin, HIGH);
  }
}

bool debounce(bool state) {
  bool stateNow = digitalRead(buttonPin);
  if (state != stateNow) {
    delay(10);
    stateNow = digitalRead(buttonPin);
  }
  return stateNow;
}
#+end_src
* delay vs millis
- ¿Cómo gestionamos el tiempo en arduino? Hasta ahora hemos utilizado =delay()=
- Para comenzar está bien, pero conforme avanzamos hay que aprender otras formas de trabajar con el tiempo.
- El delay lo utilizaremos para ralentizar lecturas y evitar rebotes
** millis()
- Devuelve número en milisegundos desde que arranca el programa
- Puede durar hasta casi 50 días (y vuelve a cero)
- variable =unsigned long= (de 0 a 4,294,967,295)
#+reveal: split
Vemos la diferencia entre usar =delay()= y =millis()=
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+begin_src arduino
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
   digitalWrite(13, HIGH);
   delay(500);
   digitalWrite(13, LOW);
   delay(500);
}
#+end_src
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+begin_src arduino
unsigned long tiempo=0;
unsigned long tiempoLed=0;
int blink=500;
boolean estado=false;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  tiempo = millis();
  if(tiempo >= (tiempoLed+blink)){
    tiempoLed=tiempo;
    estado = !estado;
    digitalWrite(LED_BUILTIN,estado);
    }
}
#+end_src
#+REVEAL_HTML: </div>
** Ejercicio en clase
Tenemos que poder encender un led si pulsamos un botón y que a la vez otro led esté parpadeando constantemente sin que se vea interrumpido
* Servomotores
#+attr_html: :height 400px :display block
#+caption: Servomotor SG90
https://uelectronics.com/wp-content/uploads/2017/06/AR0071-Servomotor-SG90-RC-9g-V6.jpg
#+reveal: split:t
- Capacidad de ubicarse en cualquier posición y mantenerla 
- lleva incorporado un sistema de regulación que puede ser controlado tanto en velocidad como en posición
- Uso de PWM para controlar dirección y posición
- Los hay de 180º y 360º
#+reveal: split:t
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: Cables servomotor
https://naylampmechatronics.com/img/cms/Blog/tutorial%20servo/Conector%20servo,%20pines%20servomotor.PNG
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :height 290px :display block
#+CAPTION: Tipos de código de color
https://i1.wp.com/www.web-robotica.com/wp-content/uploads/2015/10/colores-ServoMotor.png
#+REVEAL_HTML: </div>
** Ejemplo
#+attr_html: :height 290px :display block
#+CAPTION: Esquema de conexión potenciómetro+servo. Fuente: [[https://docs.arduino.cc/learn/electronics/servo-motors][arduino.cc]]
https://docs.arduino.cc/static/323f79da8e7a89e7d2ff9805c5976b25/a6d36/servo-knob-circuit.png
#+reveal: split:t
#+begin_src arduino
  #include <Servo.h>
  
  Servo myservo;  // create servo object to control a servo
  
  int potpin = 0;  // analog pin used to connect the potentiometer
  int val;    // variable to read the value from the analog pin
  
  void setup() {
    myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  }
  
  void loop() {
    val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
    val = map(val, 0, 1023, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
    myservo.write(val);                  // sets the servo position according to the scaled value
    delay(15);                           // waits for the servo to get there
  }
#+end_src
* Memoria EEPROM
#+attr_html: :height 290px :display block
#+CAPTION: Fuente: [[https://www.youtube.com/watch?v=ewgXDRlQe8c][Bitwise AR]]
[[../../img/11/eeprom0.png]]
#+reveal: split:t
- /EEPROM/ viene de /Electrically Erasable Programmable Read Only Memory/
- Es una memoria *no volátil*, que podemos usar para almacenar información entre reinicios
- También tenemos la memoria /FLASH/, no volátil, donde guardamos el código de Arduino
- y la /SRAM/, volátil, donde se almacenan las variables durante el funcionamiento
** Características EEPROM
- Mantiene los valores almacenados cuando se pierde la alimentación
- Arduino UNO: 1KB. MEGA: 4KB. Poca memoria 
- Tiene una vida limitada de escritura: 100.000 cada celda
- Está pensada para realizar escrituras con tiempos largos entre ellas, no un uso constante de la misma
** Tiempos de uso EEPROM
- Apenas 5 minutos si, por error, grabamos constantemente
- Aproximadamente un día, si grabamos cada segundo
- Unos 27 años si grabamos 10 veces al día
- Tiempo de escritura: 3.3ms
- Tiempo de lectura: 3.3ms
** Algunos ejemplos de la memoria EEPROM
- Guardar una contraseña personalizada
- Grabar una secuencia de valores corta en el tiempo
- Lista HighScores en un videojuego arcade
- Salvar una serie de configuraciones específicas, por ejemplo una alarma de reloj
** Esquema
#+attr_html: :height 400px :display block
#+caption: Por defecto todas las casillas vienen con valor 1 lógico ( o 255)
[[../../img/11/eeprom.png]]
** Código
- src_arduino[:exports code]{EEPROM.length();} --> Tamaño en bytes de nuestra memoria
- src_arduino[:exports code]{EEPROM.read(dirección);} --> lee el valor de la celda (dirección)
- src_arduino[:exports code]{EEPROM.write(dirección, valor);} --> escribe el valor en la celda
** Ejemplo1
#+begin_src arduino
#include <EEPROM.h>                
void setup() {
  Serial.begin(9600);                
 
  Serial.print("Capacidad de memoria: ");   
  Serial.println( EEPROM.length() );       
  Serial.println(" ");              

  Serial.print("Valor almacenado en direccion 0: ");   
  Serial.println( EEPROM.read(0) );            

  Serial.print("Almacenando numero 39 en direccion 0");  
  EEPROM.write(0, 39);                      
}

void loop() {                 
}
#+end_src
** Código
- src_arduino[:exports code]{EEPROM.get(dirección de inicio, valor);} --> Leer
- src_arduino[:exports code]{EEPROM.put(dirección de inicio, valor);} --> Escribir
- src_arduino[:exports code]{EEPROM.update(dirección de inicio, valor);} --> Actualiza. Si la celda tiene el mismo valor no sobreescribe
** Ejemplo2
#+begin_src arduino
  #include <EEPROM.h>                

float temperatura = 27.50;            
char cadena[] = "Hola esta es una prueba";   

void setup() {
  Serial.begin(9600);              
 
  EEPROM.put(0, temperatura);            
  EEPROM.put(10, cadena);           

  Serial.println("Valor de punto flotante en direccion 0:");    
  Serial.println( EEPROM.get(0, temperatura) );                   
  Serial.println(" ");                       
  Serial.println("Valor de la cadena en direccion 10:");    
  Serial.println( EEPROM.get(10, cadena) );           
}                               

void loop() {                   
}
#+end_src
** Ejercicio en clase
- Con un potenciómetro movemos un servo
- los valores de movimiento los guardamos en la eeprom para hacer un playback después de que usemos todas las direcciones de la memoria
- Importante: hacerlo todo dentro del void setup
#+begin_src processing :tangle no ../../code/11/sketch_00_eeprom/sketch_00_eeprom.ino :mkdirp yes :exports none
#include <EEPROM.h>
#include <Servo.h>

Servo servo;

int valor = 0;

void setup() {
  Serial.begin(9600);
  servo.attach(3);

  Serial.println("Comienza la secuencia de grabado EEPROM...");
  delay(500);

  for (int i = 0; i < EEPROM.length()/2; i++) {
    valor = map(analogRead(0), 0, 1023, 0, 180); //Lectura del Potenciómetro
    servo.write(valor); // al servo
    EEPROM.write(i, valor); // a la EEPROM;
    Serial.print(i, valor);
    delay(100);
  }

  Serial.println("Secuencia ESCRITURA terminada!");
  delay(3000);
  Serial.println("Comienza lectura secuencia...");
  delay(500);
  
  for (int i = 0; i < EEPROM.length()/2; i++) {
    valor = EEPROM.read(i); // a la EEPROM;
    servo.write(valor); // al servo
    Serial.print(i, valor);
    delay(100);
  }
  
  Serial.println("Secuencia LECTURA terminada!");
  delay(1000);
}

void loop() {
}
#+end_src  
* LCD
#+caption: /Liquid Crystal Display/
#+attr_html: :height 290px :display block
[[../../img/11/lcd.jpg]]
#+reveal: split:t
- Nos permite mostrar información en una pantalla de bajo coste. Monocromática. Retroiluminada
- Protocolo I2C: Interfaz serial que permite la comunicación entre dispositivos con solo dos cables
- Integración con Arduino: Uso de bibliotecas para controlar la LCD a través del protocolo I2C, simplificando la conexión y la programación
** I2C
#+caption: /Inter-Integrated Circuit/
#+attr_html: :height 290px :display block
[[../../img/11/I2C.png]]
#+reveal: split:t
- Protocolo  que facilita la interacción entre dispositivos con solo dos cables. Transmisión bidireccional
- Comunicación sincrónica: dos señales, SDA (Serial Data Line) para la transferencia de datos y SCL (Serial Clock Line) para sincronizar la comunicación entre los dispositivos conectados
- Permite la conexión de múltiples dispositivos a un mismo canal I2C, asignando a cada uno una dirección única para la comunicación
*** Averiguar la dirección I2C del LCD
- Cada fabricante graba en los LCD una dirección i2c por defecto
- Primero tenemos que indicar la placa que vamos a utilizar. En nuestro caso Arduino UNO
- Cargaremos el ejemplo que ya viene en el IDE de arduino:
- Ejemplos>>Wire>>i2c_scanner
*** Instalamos la librería
- La mayoría de displays que disponemos en la escuela tienen el driver PCF8574
- Por lo tanto trabajaremos con la librería [[https://github.com/mathertel/LiquidCrystal_PCF8574][LiquidCrystal_PCF8574]] que se puede encontrar en el gestor de librerías del IDE de arduino
- Abrimos el ejemplo básico "LiquidCrystal_PCF8574_Test"
#+reveal: split:t
- En este ejemplo podemos ver las funciones básicas para la LCD
- Si queremos crear caracteres nuevos podemos realizarlos rápidamente con [[https://maxpromer.github.io/LCD-Character-Creator/][esta herramienta web]]
* Siguiente ->                                                     :noexport:
   :PROPERTIES:
   :reveal_background: #FFCC00
   :END:
#+REVEAL_HTML: <a href="https://julianprz.gitlab.io/programacion-creativa-21-22/main/docs/01_Processing/03-formas-simples-personalizadas.html" class="r-fit-text" target="_blank">3-Formas simples / personalizadas</h2>
* Template                                                         :noexport:
** Índice
# Generar TOC
# org-reveal-manual-toc
** Indice 2 columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 50%">
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 50%">
#+REVEAL_HTML: </div>
** 2 Columnas imagen
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/02/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/02/]]
#+REVEAL_HTML: </div>
** 2 Columnas código
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+begin_src arduino
#+end_src
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+begin_src arduino
#+end_src
#+REVEAL_HTML: </div>
 
** 3 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

** 1 imagen
#+attr_html: :height 400px :display block
#+caption: 
[[../../img/02/]]

** SVG
#+HTML: <img src="https://processing.org/a9c1aea53d4f4788062d226affba4b4d/objects.svg" width="500"/>
** export processing code                                         :noexport:
#+begin_src processing :tangle no ../../code/04/sketch_00_example/sketch_00_example.pde :mkdirp yes

#+end_src
# org-babel-tangle
# Tangle the current file. Bound to C-c C-v t.
# Tangle the current code block. C-u C-c C-v C-t
# With prefix argument only tangle the current code block.

** inline processing code                                         :noexport:
# src_processing[:exports code]{;}

** Symbols
- \downarrow
  
