#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:Texto y Tipografía
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="font-size:2em" >%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><p>Escuela Superior de Diseño de Madrid</p>
#+REVEAL_INIT_OPTIONS: hash: true
#+OPTIONS: toc:nil


* Índice
- [[Listas o /Arrays/][Listas o /Arrays/]]
- [[Ejemplos][Ejemplos]]
- [[Tipografía \downarrow][Tipografía]]
- [[Texto en Processing \downarrow][Texto en Processing]]
- [[Referencias][Referencias]]
* Resumen
- Ejemplos de proyectos realizados con el uso de texto
- Aprenderemos a usar tipografías en nuestros sketches y a poder jugar con el texto. Para ello será necesario saber lo que es un =array=
* Listas o /Arrays/
- Un /array/ es una lista de elementos de cualquier tipo de dato
- Cada elemento es identificado con un *índice*
- El primer elemento está en el índice [0], el segundo en el [1] y así
- Cada array tiene una variable =length= que nos indica el número total de elementos de esa lista
#+reveal: split:t
- Importante! al comenzar por el índice 0, un array con length 5, su último índice será el 4 -> [0], [1], [2], [3], [4]
- A continuación veremos algunos ejemplos de cómo utilizar arrays
** Ejemplo: declarar e iniciar array con n elementos
#+begin_src processing
 int[] nums = new int[10]; //Declaramos e iniciamos el array de 10 elementos
 nums[2] = 127;
 println(nums[2]);
 println(nums.length);
#+end_src
** Ejemplo: Crear array con los elementos directamente
#+begin_src processing
  int[] nums = {10,23,12,332,0,76,21};
  println(nums[2] + nums[5]);
  println(nums.length);
#+end_src
** Ejemplo: Acceder a los valores de cada índice
#+begin_src processing
  int[] nums = {10,23,12,332,0,76,21};
  for(int i=0; i < nums.length; i++){
    println(nums[i]);
  }
#+end_src
** Ejemplo: Cargar valores para cada índice
#+begin_src processing :tangle yes ../../../../code/05/sketch_00_array/sketch_00_array.pde :mkdirp yes
color[] col = new color[3];
int ind = 0;

void setup() {
  size(200, 200);
  for (int i=0; i < col.length; i++) {
    col[i] = color(random(255), random(255), random(255));
  }
}

void draw() {
}
void mousePressed() {

  fill(col[ind]);
  println(col[ind]);
  println(ind);
  ellipse(mouseX, mouseY, 30, 30);
  ind++;
  if (ind == col.length) {
    ind=0;
  }
}
#+end_src
* Tipografía \downarrow
** Ejemplos
- https://www.schultzschultz.com/free-works.html
- https://www.jacobstanton.com/generative-typography-a-study/
- https://vimeo.com/177851530
** Usar tipografía
*** Tipografía del sistema
- Podemos listar las tipografías instaladas en el sistema con src_processing[:exports code]{printArray(PFont.list());}
- Y seleccionar la tipografía de la lista así: src_processing[:exports code]{PFont f = createFont("FreeMono", 64);}
- O así:  src_processing[:exports code]{PFont f = createFont(PFont.list()[23], 64);}
*** Tipografía específica
- Podemos cargar una tipo específica desde el menú "Herramientas -> Crear fuente"
- Para usarla tenemos que hacerlo de la siguiente manera:
  #+begin_src processing
    PFont font;
    void setup(){
      font = loadFont("LetterGothicStd-48.vlw");
       }
  #+end_src
- También podemos tener la tipografía en la carpeta "data" junto a nuestro sketch y cargarla así:
    #+begin_src processing
    PFont font;
    void setup(){
      font = createFont("LetterGothicStd",48);
       }
  #+end_src
** Ejemplo con tipografía 1
#+begin_src processing :tangle yes ../../../../code/05/sketch_01_type/sketch_01_type.pde :mkdirp yes
PFont f;
int w = 0;
int h = 3;
float a = 0;
char c = 'A';
void setup() {
  size(800, 800);
  background(0);
  f = createFont("Iosevka", 48); //Aquí tu tipografía favorita
  textFont(f, 200);
}

void draw() {
  background(0);
  h = int(map(mouseY, 0, height, 3, 20));
  w = int(map(mouseX, 0, width, -width/2, width/2));
  for (int i=0; i < h; i++) {
    a = a + 360/h;
    if (a >= 360) {
      a = 0;
    }
    if (360%h!=0) { //Si el resto de la división entre 360 no es 0, suma uno a h. Evita flickering
      h = h+1;
    }
    fill(255);
    textAlign(CENTER);
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(a));
    text(c, w, 0);
    popMatrix();
  }
}

void keyTyped() {
  if (key >= '0' && key <= 'z') {
    c = key;
  }
}
#+end_src
** Ejemplo con tipografía 2
#+begin_src processing :tangle yes ../../../../code/05/sketch_02_typeString/sketch_02_typeString.pde :mkdirp yes
PFont f;
int w = 0;
int h = 3;
float a = 0;
String st = "ola";
void setup() {
  size(800, 800);
  background(0);
  f = createFont("Iosevka", 48);
  textFont(f, 200);
}

void draw() {
  background(0);
  h = int(map(mouseY, 0, height, 3, 20));
  w = int(map(mouseX, 0, width, -width/2, width/2));
  for (int i=0; i < h; i++) {
    a = a + 360/h;
    if (a >= 360) {
      a = 0;
    }
    if (360%h!=0) {
      h = h+1;
    }
    fill(255);
    textAlign(CENTER);
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(a));
    text(st, w, 0);
    popMatrix();
  }
}

void keyTyped() {
  if (key >= '0' && key <= 'z') {
    st = st + key;
  }

  if (st.length() >= 1) {
    if (key ==BACKSPACE) {
      st = st.substring(0, st.length()-1);
    }
  }
}
#+end_src
* Texto en Processing \downarrow
** Ejemplos
- Rafael Lozano-Hemmer:
https://vimeo.com/491273274
https://vimeo.com/328474328
https://vimeo.com/441516393
https://vimeo.com/392074176

Daniel Canogar:
https://vimeo.com/410150948
https://vimeo.com/250800449
https://vimeo.com/377304919

** String separado por caracteres
#+begin_src processing :tangle yes ../../../../code/05/sketch_03_textChars/sketch_03_textChars.pde :mkdirp yes
//Mostramos texto en canvas, listado de fuentes instaladas, creamos un segundo texto en el que modificamos caracteres individualmente

size(600,400);
background(0);

PFont f = createFont("FreeMono", 64);
String s = "Ola ke ase?";

textFont(f);
textSize(64);
text(s,10,100);

float x = 10;
for (int i=0; i < s.length(); i++){
  char c = s.charAt(i);
  //textSize(random(32,100));
  fill(random(25,255));
  text(c, x, 300);
  x += textWidth(c);
}
#+end_src
** String separado por palabras
#+begin_src processing :tangle yes ../../../../code/05/sketch_04_textWords/sketch_04_textWords.pde :mkdirp yes
//Utilizamos split() para partir el string en palabras

size(600,400);
background(0);

PFont f = createFont("FreeMono", 64);
String s = "Ola ke ase?";

String[] words = split(s, " ");
//printArray(PFont.list());

textFont(f);
textSize(64);
//textAlign(CENTER);
text(s,10,100);

float x = 10;
for (int i=0; i < words.length; i++){
  //char c = s.charAt(i);
  textSize(64);
  fill(random(25,255));
  text(words[i], x, 300);
  //x += textWidth(words[i]+1); //Espaciado por pt
  x += textWidth(words[i])+40; //Espaciado por pixeles
}
#+end_src
** Valores separados por coma
#+begin_src processing :tangle yes ../../../../code/05/sketch_05_textValues/sketch_05_textValues.pde :mkdirp yes
//Utilizamos split() para partir el string en palabras e int() para pasar tipo de datos de string a enteros

size(600,400);
background(0);

PFont f = createFont("FreeMono", 32);
String s = "100,50,25,75,14";

String[] nums = split(s, ","); //Esta sería la forma de separar valores de un archivo CSV (Comma Separated Values)

int[] vals = int(nums);

textFont(f);
textAlign(CENTER,CENTER);
noStroke();

translate(100,0);
for (int i=0; i < vals.length; i++){
  ellipse(i*100,height/2,vals[i],vals[i]);
  //rect(i*100-10,height/2,20,-vals[i]);
  text(nums[i], i*100,height*3/4); //Podemos utilizar en array nums de tipo String para mostrar los valores como texto
}
#+end_src
** Cargando un archivo de texto
#+begin_src processing :tangle yes ../../../../code/05/sketch_06_textLoad/sketch_06_textLoad.pde :mkdirp yes
// Cargamos un archivo .txt y lo dividimos en líneas de texto. Después unimos todas las lineas en un único string de palabras,
// y después creamos un array de todas las palabras separadas para mostrarlas en pantalla individualmente

String[] words;
int index = 0;

void setup() {
  size(600, 400);
  frameRate(5);
  background(0);
  String[] lines = loadStrings("quijote.txt");
  //printArray(lines);
  String entireText = join(lines, " ");
  //print(entireText);
  words = splitTokens(entireText, ",.?!:; ");
  //printArray(words);
}

void draw() {
  background(0);
  fill(255);
  textSize(64);
  textAlign(CENTER);
  text(words[index],width/2, height/2);
  index++;
}
#+end_src
* Referencias
- [[https://youtu.be/SKDhkB8g1So][Kinetic Typography (tutorial)]]
- [[https://www.creativeapplications.net/processing/generative-typography-processing-tutorial/][Curso de tipografía generativa]] (requiere registro)
- Libro [[https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn][Type+Code]]: Processing for designers

* Siguiente ->                                                     :noexport:
   :PROPERTIES:
   :reveal_background: #FFCC00
   :END:
#+REVEAL_HTML: <a href="https://julianprz.gitlab.io/programacion-creativa-21-22/main/docs/01_Processing/06-Imagen.html" class="r-fit-text" target="_blank">6- Imagen y Píxeles</h2>
* Contenidos                                                       :noexport:
* Template                                                         :noexport:
** Índice
# Generar TOC
# org-reveal-manual-toc
** Indice 2 columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 50%">
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 50%">
#+REVEAL_HTML: </div>
** 2 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/02/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/02/]]
#+REVEAL_HTML: </div>
** 3 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

** 1 imagen
#+attr_html: :height 400px :display block
#+caption: 
[[../../img/02/]]

** export processing code                                         :noexport:
#+begin_src processing :tangle no ../../code/04/sketch_00_example/sketch_00_example.pde :mkdirp yes

#+end_src
# org-babel-tangle
# Tangle the current file. Bound to C-c C-v t.
# Tangle the current code block. C-u C-c C-v C-t
# With prefix argument only tangle the current code block.

** inline processing code                                         :noexport:
# src_processing[:exports code]{PFont f = createFont("FreeMono", 64);}

** Symbols
- \downarrow
  
