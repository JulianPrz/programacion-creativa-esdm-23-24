#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:Funciones y Variables
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="font-size:2em" >%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><p>Escuela Superior de Diseño de Madrid</p>
#+REVEAL_INIT_OPTIONS: hash: true
#+OPTIONS: toc:nil

* Índice
#+REVEAL_HTML: <div class="column" style="float:left; width: 50%">
  - [[Introducción a Processing][Introducción a Processing]]
  - [[Entorno de desarrollo][Entorno de desarrollo]]
  - [[Consola][Consola]]
  - [[Sketchbook][Sketchbook]]
  - [[Píxel][Píxel]]
  - [[Estilos de programación \downarrow][Estilos de programación]]
  - [[Renderers][Renderers]]
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 50%">
  - [[Color][Color]]
  - [[Tipo de datos (más comunes)][Tipo de datos]]
  - [[*Conversores][Conversores]]
  - [[Variables del sistema][Variables del sistema]]
  - [[Posición][Posición]]
  - [[Pantalla][Pantalla]]
  - [[Ejercicio][Ejercicio]]
  - [[Editores externos \downarrow][Editores externos]]
#+REVEAL_HTML: </div>

* Introducción a Processing
- Herramienta orientada a enseñar a programar en un contexto visual a usuarixs no programadores en el ámbito de las artes visuales, diseño gráfico, arquitectura, etc.
- Librería gráfica e IDE (/Integrated development environment/)
- Software Libre
- Multiplataforma (/Windows/, /Mac OS/, /Gnu/Linux/)
#+REVEAL: split:t
- Basado en /Java/
- Precursor de /Arduino/, /Fritzing/, /p5js/
- Creadores: Ben Fry & Casey Reas. 2001
- Processing Foundation
- Gran comunidad
- → [[https://processing.org/download][Descargar programa]]

* Entorno de desarrollo
#+attr_html: :height 400px :display block
#+CAPTION: IDE (/Integrated development environment/) de Processing
https://upload.wikimedia.org/wikipedia/commons/0/08/Processing_4.0b1_Screenshot.png

#+REVEAL: split:t
- El /IDE/ transforma nuestro código a lenguaje máquina: ceros y unos. A esto se le llama "compilar"
- En él gestionamos las librerías y herramientas, y accedemos a las diferentes opciones del programa
- Tenemos una [[*Consola][consola]] para ver los avisos y errores de nuestro código al compilar

#+REVEAL: split:t
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :display block
[[../../img/02/play.png]]

Botón para *ejecutar* nuestro código. Atajo: Cmd/Ctrl+R
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :display block
[[../../img/02/stop.png]]

Botón para *parar* nuestro sketch. Es mejor parar el sketch que cerrar la ventana del canvas
#+REVEAL_HTML: </div>
* Consola
  - La consola es ese rectángulo negro que hay en la parte inferior del IDE
  - En este área el programa nos devuelve datos que nuestro programa está produciendo
  - Con [[https://processing.org/reference/println_.html][=println()=]] podemos escribir datos a la consola
  - Es realmente útil para consultar el valor de una operación o la info que tiene almacenada una variable
  - Podemos imprimir mensajes para saber si una función se está ejecuntando correctamente
  #+reveal: split:t
Algunos ejemplos
  #+begin_src processing
  println("Hola mundo!"); //Podemos imprimir por consola mensajes aka cadena de caracteres aka strings
  #+end_src
  #+begin_src processing
  println(3+2); //Podemos hacer cálculos si trabajamos con números
  #+end_src
  #+begin_src processing
  print("El resultado de 3+2 es igual a: "+(3+2)) // o una combinación de ambas
  #+end_src
  - Es importante en este último ejemplo hacer la suma entre paréntesis porque sino contatenaría ambos numeros. Resultaría así: El resultado de 3+2 es igual a: 32
* Sketchbook
  - Processing crea una carpeta llamada "/Sketchbook/" donde guarda nuestros avances
  - La nomenclatura por defecto es "=sketch_=" + formato fecha =aammdd= + =letra=
  - De esta manera tendremos ordenados cronológicamente nuestros avances
#+reveal: split:t
  - *Importante!* El archivo siempre tiene que estar en una carpeta contenedora con el mismo nombre que el archivo
  - Por ejemplo:
    - Carpeta: =sketch_220211=
      - Archivo: =sketch_220211.pde=
  - El archivo no puede comenzar con número ni guión medio
  - Para vista de sketches en el menú /"Archivo>>Sketchbook"/
* Tip!
  - Las funciones que veáis resaltadas en color rosa, como por ejemplo [[https://processing.org/reference/size_.html][=size()=]], están enlazadas a su correspondiente explicación en el [[https://processing.org/reference][manual de referencia]] de Processing
  - Es importante consultar la referencia cuando no sabemos cómo utilizar una función
  - Está bien también para saber cuántos y cuáles argumentos acepta una función
* Píxel
  - Unidad mínima homogénea de color que forma parte de una imagen digital en pantalla
  - En Processing es un elemento con el que trabajaremos constantemente
  - Para definir el tamaño de nuestro canvas (lienzo) lo hacemos con la función [[https://processing.org/reference/size_.html][=size()=]]
  #+begin_src processing
  size(300,300);
  #+end_src 
* Estilos de programación \downarrow
** 🧍🏽‍♀️Estático
   #+begin_src processing
   size(200, 200);
   background(255);
   noStroke();
   fill(255, 204, 0);
   rect(30, 20, 50, 50);
   #+end_src
   - Con pocas líneas podemos ver lo que queremos
   - No permite la animación
   - Está bien para quien comienza a programar
** 🏃🏽‍♀️Activo
   #+begin_src processing
void setup() { // Lo que hay dentro se ejecuta una sóla vez
  size(200, 200);
  background(255);
  noLoop();
}
void draw() { // Lo que hay dentro se ejecuta en bucle infinito
  twoCircles(40, 80);
  twoCircles(90, 70);
}
void twoCircles(int x, int y) { // Se ejecuta cuando es llamada
  noStroke();
  fill(0, 102, 153, 204);
  ellipse(x, y, 50, 50);
  ellipse(x+20, y+20, 60, 60);
}
   #+end_src
- Tanto [[https://processing.org/reference/setup_.html][=setup()=]] como [[https://processing.org/reference/draw_.html][=draw()=]] son funciones propias de Processing
- Sin embargo, =twoCircles()= es una función personalizada
* Renderers
  - El renderer por defecto es =P2D= para dibujar formas en 2 dimensiones. Es rápido pero menos preciso.
  - =P3D= es para geometrías 3D. Controla la cámara, las luces y materiales.
  - =P2D= y =P3D= son acelerados si tu computadora tiene una tarjeta gráfica /OPENGL/ compatible.
  - Lo indicamos en el tercer argumento de la función [[https://processing.org/reference/size_.html][=size()=]]
  #+begin_src processing
  size(300,300,P3D); // ( ancho del canvas, alto del canvas, motor renderizado o renderers )
  #+end_src 
* Color
#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :display block
#+caption: Escala de grises
https://processing.org/440ee3e47245e39066409dfd3a53a8a9/grayscale.svg
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :display block
#+CAPTION: Sistema RGB
https://processing.org/static/5a9b1c205796435bf8e24432b2fa43c2/8ba2d/rgb.jpg
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 33%">
#+attr_html: :display block
#+CAPTION: Sistema HSB
https://processing.org/static/41dbd54a9117c1f3545e4ef12c3b4773/00d43/hsb.png
#+REVEAL_HTML: </div>

#+reveal: split:t
#+begin_src processing
  fill(128,0,255,50); // (r,g,b,a);
#+end_src
- Se expresan por combinación de tres canales con valores de 0 a 255 *(RGB)* o de 0 a 100 *(HSB)*
- *Escala de grises* un sólo canal de 0 a 255
- Se puede utilizar *alpha* como segundo valor en escala de grises o cuarto en RGB y va de 0 a 100
- Otro sistema de color es el *hexadecimal*. Comunmente utilizado en web. Se indica así: =#FF3300=
#+reveal: split:t
- Funciones que esperan argumentos de color:
  - [[https://processing.org/reference/background_.html][=background()=]] color de fondo. Por defecto si no se indica nada es gris
  - [[https://processing.org/reference/fill_.html][=fill()=]] para el relleno de formas. Por defecto es blanco
  - [[https://processing.org/reference/stroke_.html][=stroke()=]] para el trazo de formas. Por defecto es negro
  - [[https://processing.org/reference/color_.html][=color()=]] crea colores para almacenar en variables de tipo de dato [[https://processing.org/reference/color_datatype.html][=color=]]
#+reveal: split:t
- Otras funciones que eliminan estas propiedades serían:
  - [[https://processing.org/reference/noFill_.html][=noFill()=]] sin relleno
  - [[https://processing.org/reference/noStroke_.html][=noStroke()=]] sin trazo
- Para definir el ancho del trazo en píxeles usamos [[https://processing.org/reference/strokeWeight_.html][=strokeWeight()=]]
#+reveal: split:t
- Con [[https://processing.org/reference/colorMode_.html][=colorMode()=]] podemos definir el rango de los canales y cambiar el modo de color
#+begin_src processing
colorMode(RGB,1.0); //Define el rango para cada canal entro 0.0 y 1.0
#+end_src
#+begin_src processing
colorMode(HSB,360,100,100); //Cambia a modo HSB:Hue,Saturation,Brightness (Tono,Saturación,Brillo) y define rangos para cada canal.
#+end_src
* Tipo de datos (más comunes)
- El símbolo === no significa "igual a", sino que con él *asignamos* un valor a una variable
- Las variables las usamos para almacenar valores y pueden contener datos de diferentes tipos
#+begin_src processing
  int x = 0; // declaramos la variable "x" de tipo "número entero" y le asignamos el valor "0" (Sólo acepta números entre 2,147,483,647 y -2,147,483,648, 32bits)
  float velocidad = 90.58; // declaramos la variable "velocidad" de tipo "número flotante/decimal" y le asignamos el valor "90.58"
  boolean encendido = false; // declaramos la variable "encendido" de tipo "booleana" y le asignamos el valor "false"
  char caracter1 = 'a'; // declaramos la variable "caracter1" de tipo "caracter" y le asignamos el valor "a"
  String frase1 = "ola k ase"; // declaramos la variable "frase" de tipo "cadena de caracteres" y le asignamos el valor "ola k ase"
  color color1 = color(204, 153, 0); // declaramos la variable "color1" de tipo "color" y le asignamos el valor "color(204, 153, 0);"
  byte a = 23; //declaramos la variable "a" de tipo "byte" y le asignamos el valor 23 (Sólo acepta números entre 127 y -128, 8bits)
#+end_src
#+reveal: split:t
- En /"Archivo>>Ejemplos>>Basics>>Data"/ podéis encontrar varios ejemplos que trabajan con cada tipo de dato visto aquí
* Conversores
- Los conversores nos permiten transformar un tipo de dato a otro.
- A veces ciertas funciones solo reciben un tipo de dato y tenemos que hacer una conversión previa
#+reveal: split:t
- [[https://processing.org/reference/intconvert_.html][=int()=]] puede convertir [[https://processing.org/reference/boolean.html][=boolean=]], [[https://processing.org/reference/byte.html][=byte=]], [[https://processing.org/reference/char.html][=char=]], [[https://processing.org/reference/color_datatype.html][=color=]], [[https://processing.org/reference/float.html][=float=]]
- [[https://processing.org/reference/floatconvert_.html][=float()=]] puede convertir [[https://processing.org/reference/int.html][=int=]] y [[https://processing.org/reference/String.html][=String=]] (para String tiene que ser una representación de un número decimal: "123.38")
- [[https://processing.org/reference/strconvert_.html][=str()=]] puede convertir [[https://processing.org/reference/boolean.html][=boolean=]], [[https://processing.org/reference/byte.html][=byte=]], [[https://processing.org/reference/char.html][=char=]], [[https://processing.org/reference/float.html][=float=]], [[https://processing.org/reference/int.html][=int=]]
- [[https://processing.org/reference/booleanconvert_.html][=boolean()=]] puede convertir [[https://processing.org/reference/int.html][=int=]], [[https://processing.org/reference/String.html][=String=]]
- [[https://processing.org/reference/hex_.html][=hex()=]] puede convertir  [[https://processing.org/reference/int.html][=int=]] , [[https://processing.org/reference/byte.html][=byte=]], [[https://processing.org/reference/char.html][=char=]], [[https://processing.org/reference/color_datatype.html][=color=]] a un [[https://processing.org/reference/String.html][=String=]] que contiene el equivalente hexadecimal
* Variables del sistema
- [[https://processing.org/reference/width.html][=width=]] y [[https://processing.org/reference/height.html][=height=]] nos devuelven el ancho y alto de nuestro sketch
  #+begin_src processing
 point(width/2,height/2); //punto de 1x1 pixel en el centro del canvas
  #+end_src
- [[https://processing.org/reference/mouseX.html][=mouseX=]] y [[https://processing.org/reference/mouseY.html][=mouseY=]] nos devuelve la posición horizontal y vertical del puntero del ratón
  #+begin_src processing
 line(0,0,mouseX,mouseY);
  #+end_src
- [[https://processing.org/reference/frameCount.html][=frameCount=]] nos devuelve el número de frames mostrados desde que se inició el sketch
  #+begin_src processing
 println(frameCount); //Podemos utilizarlo como contador en nuestro sketch
  #+end_src
#+reveal: split:t
- [[https://processing.org/reference/keyPressed.html][=keyPressed=]] nos devuelve /true/ si una tecla es pulsada
- [[https://processing.org/reference/mousePressed.html][=mousePressed=]] lo mismo pero con el click del ratón
- [[https://processing.org/reference/key.html][=key=]] nos dice la última tecla que ha sido pulsada
  #+begin_src processing
    void draw(){
      println(keyPressed);
    }
  #+end_src
* Posición
  #+HTML: <img src="https://processing.org/8b2351f517591946126c7521dc415b25/drawing-05.svg" width="500"/>
- El sistema de coordenadas en Processing es diferente al cartesiano que estamos acostumbradxs
- Aquí en el ejeY los valores positivos van hacia abajo y los negativos hacia arriba
* Pantalla
Tenemos una serie de funciones y variables que nos permiten ajustar nuestro sketch a la pantalla
- [[https://processing.org/reference/displayWidth.html][=displayWidth=]] y [[https://processing.org/reference/displayHeight.html][=displayHeight=]] nos devuelven el ancho y alto de nuestra pantalla
- =pixelDensity(displayDensity());= Con estas dos funciones anidadas el sketch quedará ajustado a la densidad de píxeles de nuestra pantalla
- Con =fullScreen()= podemos ejecutar nuestro sketch a pantalla completa, expandir a varias pantallas o indicar en qué pantalla mostrarlo a pantalla completa
* Ejercicio
- Con lo visto hasta ahora vamos a hacer un pequeño ejercicio. El resultado tiene que ser como el de la imágen
[[../../img/02/sketch1.png]]
- Podemos realizarlo en estilo estático
- 2 líneas diagonales, cada una de un color diferente
- y 2 líneas del mismo color formando una cruz
- Podéis aumentar el grosor de la línea
* Ejemplos
  - Para ver las posibilidades de Processing os recomiendo que exploréis los ejemplos que vienen con la instalación del programa
  - Los podéis encontrar en /"Archivo>>Ejemplos"/
  - Otra plataforma para ver lo que hace otra gente es [[https://www.openprocessing.org/][Open Processing]]
  - También es un buen sitio donde publicar tus sketches de Processing
* Editores externos \downarrow
** Visual Code
- Recomiendo la extensión /Processing VSCode/ de Luke-zhang-04 (más actualizada)
- En /macOS/ hay que instalar /processing-java/ desde el menú /Herramientas/
- En /Windows/ tienes que configurar el directorio de Processing en el /path/ del sistema
  - Puedes realizar estos pasos según [[https://www.youtube.com/watch?v=LKuu-WcOZYA][este video]]
** Sublime text
- Puedes seguir los pasos explicados en [[https://github.com/b-g/processing-sublime][este repo]]
- Si tienes Sublime 4 tienes que clonar o descargar manualmente el proyecto en la carpeta /Packcages/ de SublimeText
* Siguiente ->                                                     :noexport:
   :PROPERTIES:
   :reveal_background: #FFCC00
   :END:
#+REVEAL_HTML: <a href="https://julianprz.gitlab.io/programacion-creativa-21-22/main/docs/01_Processing/03-formas-simples-personalizadas.html" class="r-fit-text" target="_blank">3-Formas simples / personalizadas</h2>
* Contenidos                                                       :noexport:
* Template                                                         :noexport:
** Índice
# Generar TOC
# org-reveal-manual-toc
** 2 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/02/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/02/]]
#+REVEAL_HTML: </div>
** 3 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

** 1 imagen
#+attr_html: :height 400px :display block
#+caption: 
[[../../img/02/]]

** export processing code
#+begin_src processing :tangle code/s1/example.pde :mkdirp yes

#+end_src
